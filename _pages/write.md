---
layout: content
title: Write
permalink: /write/
---

Check our [demo](https://gitlab.com/pandemics/demo) to have a practical example of the formatting possibilities of Pandemics.

# Text formatting

You can format text (eg. titles, italics, lists, etc.) using the [Markdown](https://guides.github.com/features/mastering-markdown/) syntax as defined in [Pandoc](https://pandoc.org/MANUAL.html#tables). You can find a lot of resources on the web to learn Markdown, like [this tutorial](https://commonmark.org/help/). Don't worry, it will only take you a couple of minutes.

Example
{: .example}

manuscript.md
{: .filename}
```
# A section title
Lorem ipsum.

## A subsection title
- An item in *italic*
- A **bold** item
```

manuscript.pdf
{: .filename}
> <h2 class="no_toc">A section title</h2>
> Lorem ipsum.
>
>
> <br/>
>
> <h3 class="no_toc">A subsection title</h3>
> - An item in *italic*
> - A **bold item**

[back to top](#)

# Comments

You can prevent any part of your manuscript from being displayed in the compiled document by wrapping it in in the special `<!--` ... `-->` markup. This allows you eg. to write comments that the authors can read but will not appear in the published version.

Example
{: .example}

manuscript.md
{: .filename}

```
A short sentence, <!-- We need to be more specific here  --> and another one.
```

manuscript.pdf
{: .filename}

> A short sentence, <!-- We need to be more specific here  --> and another one.

[back to top](#)

# Figures

You can integrate images in your document using the classic Markdown syntax:

```markdown
<!-- A simple figure with a legend -->
![The image legend](relative-path-to/the-image.png)
```

The path should be given relative to your document.


You can also associate the image with a label. It has to be put into curly brackets just after the image and start with `#fig:`. You can then refer to this label in the text using `@fig:thelabel` and Pandemics will replace all the references with the correct numbering.

```markdown
<!-- A simple figure with a label for cross-referencing. -->
![The image legend](relative-path-to/the-image.png){#fig:my-label}

<!-- A cross-reference to the figure -->
See figure @fig:my-label.
```

Some output formats also accept the specification of image attributes, eg.:

```markdown
<!-- Set the image width to 60% of the page / div -->
![The image legend](relative-path-to/the-image.png){width=60%}
```

You can of course combine the different attributes:

```markdown
![The image legend](relative-path-to/the-image.png){#fig:my-label width=60%}
```

[back to top](#)

# Citations

You can insert references in your markdown document using the [markdown citation syntax](https://rmarkdown.rstudio.com/authoring_bibliographies_and_citations.html#citations). By default, Pandemics will look for a `bibliography.bib` bib file in the source folder to parse the references. You can however override this default by specifying the relative path to you .bib files in the front-matter:

```yaml
---
bibliography: myReferences.bib
---
```

You can also provide multiple .bib files if needed:

```yaml
---
bibliography:
  - myReferences.bib
  - ../shared/references.bib
---
```

Example
{: .example}

manuscript.md
{: .filename}

```markdown
As demonstrated by @doe2018, ...

# References
<!-- Reference list will be automatically added at the end of the document -->
```

bibliography.bib
{: .filename}

```
@article{doe2018,
  author={Doe, John},
  year={2018},
  title={A fantastic paper}
}
```

manuscript.pdf
{: .filename}
> As demonstrated by Doe (2018), ...
>
> <br/>
>
> <h2 class="no_toc">References</h2>
>
> Doe, John. 2018. “A Fantastic Paper.”

**Zotero integration (experimental)**

If you have [Zotero](https://www.zotero.org/) installed with the [betterBibTex plugin](https://retorque.re/zotero-better-bibtex/installation/), you can ask Pandemics to update your .bib file automatically from your Zotero library. In the header of your markdown document, set `autobib: true` as a pandemics option:


```yaml
---
pandemics:
  autobib: true
---
```

Note that the Zotero app must be running at the time of compilation for this to work.

If you do not specify a bibliography file in the front-matter of your document, Pandemics will create a default one. Otherwise, Pandemics will combine all the listed  .bib files and the Zotero entries and save the merged bibliography in the first .bib file provided in the front-matter.

If you're working within Atom, the [Zotero Picker](https://atom.io/packages/zotero-picker) makes a perfect companion.

[back to top](#)

# Equations

You can write equations using the [LaTeX syntax](https://en.wikibooks.org/wiki/LaTeX/Mathematics). You would just need to enclose your mathematical expression either between `$` ... `$` (inline mode) or `$$` ... `$$` (bloc mode).

As for figures, you associate a label, this time starting with `#eq:`, to your equation so you can them refer to it and generate an automatic equation numbering:

Example
{: .example}

manuscript.md
{: .filename}

```
$$ \beta = a^2 $${#eq:my-equation}

See equation @eq:my-equation.
```
manuscript.pdf
{: .filename}

> $$ \beta = a^2 \qquad(1)$$
>
> See equation 1.

[back to top](#)

# Tables

The best way to include a table is to import it from an external file in .csv format:

{% raw  %}
```markdown
{{ #csv ./path-to/my-table.csv }}
```
{% endraw  %}

Of course, you can also enter your table manually using the [markdown syntax](https://pandoc.org/MANUAL.html#tables). Either way, you can add after your table a caption using `Table:` in the line immediately following your table. Like for figures and equations, tables cab be labelled for cross-referencing using the `#tbl:`prefix.

Example
{: .example}

manuscript.md
{: .filename}
{% raw  %}
```
{{ #csv ./path-to/my-table.csv }}
Table: A dynamically loaded table {#tbl:dynamic}

See table @tbl:dynamic.
```
{% endraw  %}

my-table.csv
{: .filename}

```
, Header 1, Header 2
Variable 1, Value A, Value B
Variable 2, Value C, Value D
```

manuscript.pdf
{: .filename}

> | | Header 1 | Header 2 |
> | --- | -------- | -------- |
> | Variable 1 | Value A  | Value B  |
> | Variable 2 | Value C  | Value D  |
>
> <div style="text-align: center;">Table 1. A dynamically loaded table</div>
>
> <br/>
>
> See table 1.

## Options (experimental)

You can pass options after the file path to alter how a .csv file is loaded and displayed:

{% raw  %}
```markdown
{{ #csv ./path-to/my-table.csv align=lcr colsize=2:1:1 }}
```
{% endraw  %}

Options are:

- `align`: a sequence of `l`, `c`, or `r` to align each column to respectively to the left, center, or right.
You must provide as many flags are there are columns in the table.
- `colsize`: relative size of the columns, when the table is too large and has to be displayed in full page width.
You must specify one number per column, separated by a colon (`:`). For example, `1:3` will scale two columns to take 25% and 75% of the page width respectively.
- any option described in the [csv-parse documentation](https://csv.js.org/parse/options/), passed as `key=value`.

[back to top](#)

# Live results

You can integrate values or elements that have been exported to `json` or `yaml` using [Mustache](https://mustache.github.io/mustache.5.html) templating in your document. This way, you can be sure that you compiled document will always be up-to-date.

To do so, you simply need to specify one or multiple `.json` or `.yaml` as the `mustache` option in the yaml front matter of your document.

```yaml
---
mustache: myResults.json
---
```

or

```yaml
---
mustache:
  - myResultsA.yaml
  - myResultsB.json
---
```

Pandemics will parse those files and make the values they define available as variable you can integrate using the mustache template {% raw %}`{{key}}`{% endraw %} where `key` is the label of the value in the result file. You can load nested value using the dot notation: {% raw %}`{{key1.key11}}`{% endraw %}.



Example
{: .example}

results.json
{: .filename}

```json
{
  "exp1": {
    "beta": 2.80,
    "pvalue": 0.036
  }
}
```

manuscript.md
{: .filename}

{% raw %}
```
There was an effect ($\beta$ = {{ exp1.beta }}, p = {{ exp1.pvalue }}).
```
{% endraw %}


manuscript.pdf
{: .filename}

> There was an effect of treatment (&beta;  = 2.80, p = 0.036).

## Conflicting entries

If multiple input files are provided, it might happen that the same entry is defined twice, which will throw an error. For example, if `result1.json` and `result2.json`  both define (at root) the key `pvalue`. A workaround is to prefix the values loaded from (at least) on of the file, eg:

```yaml
---
mustache:
- result1.json
- prefix: replication
  file: result2.json
---
```

The respective `pvalue` values will then be available in your mustache template as {% raw %}`{{pvalue}}`{% endraw %} and {% raw %}`{{replication.pvalue}}`{% endraw %} respectively.

[back to top](#)

# Multiple files documents

If your working on a large project, you might want to split your manuscript in multiple files (eg. chapters).
As for tables and code, you can include those file in your main document in a single line, Pandemics will take care of merging everithing

Example
{: .example}

manuscript.md
{: .filename}
{% raw  %}
```
{{ #include chapters/*.md }}
```
{% endraw  %}

chapters/chapter-1.md
{: .filename}

```
# A title

A paragraph.
```

chapters/chapter-2.md
{: .filename}

```
# Another title

Another paragraph.
```

manuscript.pdf
{: .filename}

> <h2 class="no_toc">A title</h2>
>
> A paragraph.
>
> <br />
>
> <h2 class="no_toc">Another title</h2>
>
> Another paragraph.

If you want to further include other elements (images, code, tables, other markdown documents) inside the nested document (*eg.* from `chapters/chapter-2.md`), the path to the element must be provided relative to the included document. For example, if you include an image with `![](./images/my-image.jpg)` from within `chapters/chapter-2.md`, Pandemics will look for `my-image.jpg` in the `chapters/images/` folder.

[back to top](#)

# Listings

## Inline code

Short snippets of code can be displayed in "code style fonts" by wrapping them between single `` ` `` (back-ticks):

Example
{: .example}

manuscript.md
{: .filename}
```
Please see function `myfunction()`.
```

manuscript.pdf
{: .filename}

> Please see function `myfunction()`.

## Code blocs


For longer, multi-lines pieces of code, wrap your paragraph with 'fences' of `` ` ```` ` ```` ` `` (triple back ticks):

    <!-- This is a code bloc -->
    ```
    console.log('Hello World!');
    ```


Better, you can directly load a script to include in as a listing in your document.

{% raw  %}
    {{ #code some-code.py }}
{% endraw  %}

In both cases, you can add a title and/or a label to your code block for later cross-referencing.

Example
{: .example}

manuscript.md
{: .filename}
{% raw  %}
    <!-- A code block -->
    ```
    console.log('Hello World!');
    ```
    Listing: A simple hello world example.{#lst:hello-js}

    <!-- Another code block using an external file -->
    {{ #code some-code.py }}
    Listing: A python example.{#lst:hello-py}

    See the examples @lst:hello-js and @lst:hello-py.
{% endraw  %}

some-code.py
{: .filename}

```
print("I am Py.")
```

manuscript.pdf
{: .filename}

> ```
> console.log('Hello World!');
> ```
> **Listing 1.** A simple hello world example.
>
> <br/>
>
> ```python
> print("I am Py.")
> ```
> **Listing 2.** A python example.
>
> See the examples *Listing* 1 and *Listing* 2.

## Syntax highlighting

Depending on the recipe, your code will be automatically coloured using syntax highlighting. To specify the language grammar to use for this highlighting, provide the extension associated to the snippet directly after the initial triple backtick of a code bloc:

    ```js
    console.log('Hello World!')
    ```

Pandemics will automaticall try to guess the language of code included via a file import. To override the default behaviour, you can pass the desired language as an option:

{% raw  %}
    {{ #code some-code.noext syntax=python }}
{% endraw  %}


[back to top](#)

# Strict mode

By default, if you try to include elements (images, tables, mustache values, etc.) form external files which cannot be found, Pandemics will print an error message in the output document where the included element should have been.
This behaviour allows for missing parts when generating a document, for example when writing a report while some results are still incomplete.

You can however request Pandemics to compile in strict mode, in which case the compilation will fail if any element is missing. A typical use case would be to check that a report is absolutely complete before submission, or as a quality check when the compilation runs in automated scripts.

To enable strict mode, simple set the `strict` option to `true` in the front-matter of your document:


```yaml
---
pandemics:
  strict: true
---
```

[back to top](#)

# Versioning

If your document is under version control with git, Pandemics will provide the template variable `$sourceSHA` and,
when using LaTeX based export (e.g. pdf), the command `\theversion` which both contain the SHA of the current commit of the enclosing repo. If the repo is not in a clean state, eg when there are non commited changes, both variables will be empty.

[back to top](#)

# Raw LaTeX


You can include raw latex elements in your markdown document, including in the front matter:

```yaml
---
date: \today
---

Lorep ipsum.

\newpage
```

Beware however that your LaTeX snippet cannot contain `{% raw  %}{{{% endraw  %}` ... `}}` sequences as it would interfere with mustache templating. As a work around, you can either:
- ensure you put at least one space between consecutive curly braces, i.e. use `{ {` instead of `{% raw  %}{{{% endraw  %}`
- temporarily change the mustache delimiters to another tag before your Latex snippet and set it back after. For example, to set the mustache tags to `<% %>`:


{% raw  %}
  ```markdown
  <!-- set mustache tags to erb style: <% %>-->
  {{=<% %>=}}

  ... some long latex snippet including {{ and }}

  <!-- switch back to default mustache tags: {{ }} -->
  <%={{ }}=%>
  ```
{% endraw %}

[back to top](#)



# And more

Pandemics can also interpret other formatting tricks, like footnotes (`^[This is a footnote]`), links (`[inline link](url)`), format specific markup (eg. HTML spans and divs with associated class), etc. Checkout the [pandocs markdown](https://pandoc.org/MANUAL.html#pandocs-markdown) documentation for an exhaustive list of all the features supported by Pandemics.

[back to top](#)
