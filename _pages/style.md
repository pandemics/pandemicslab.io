---
layout: content
title: Style with recipes
permalink: /style/
---

By default, Pandemic will use a standard set of template, filters, and options that should cover the usual needs for manuscript publication (citations, etc.). However, as you've seen in the [tutorial]({{ site.baseurl }}/tutorial), you can instantly change the style of the compiled document by using a _recipe_.

In practice, a _recipe_ is a set of files and instructions, stored in a git repository, which provides Pandemics with all the informations needed to compile a document. This page explains how you can tailor your perfect styling template and create your own reusable recipe.

# Overview

## Structure of a recipe

The core of Pandemics is the [Pandoc](https://pandoc.org/MANUAL.html) file converter.
A Pandemics recipe encapsulates all the elements necessary for running Pandoc: template, options, filters, plus some extra processing steps if needed.
This way, your working directory needs to contain only the pure content, without being cluttered by styling definitions or esoteric Makefiles.

The typical recipe will contain two things:
- A Pandoc template file, which defines the general layout and appearance of the document;
- An JSON file named `recipe.<format>.json`, which describes which Pandoc options and filters to use, and specify eventual preprocessing steps.

A recipe does not need to necessarily have those two elements.
If only the instructions are provided, Pandemics will use a default template corresponding to the requested format. If a (single) template is provided, Pandemics will use it with some default options.

## Output format of a recipe

If the recipe contains a unique instructions file, or a single template, Pandemics will automatically select the format of the output document accordingly.

However, a recipe can also support multiple formats (eg. PDF or HTML, or a draft template and a final template).
In that case, the recipe must contain one instructions file for each output format in addition to the templates, if any.

[back to top](#)

# Template

A template is a document which defines the layout and styling of your output document. Templates can also contain placeholders that will be replaced by your content and the variables defined in header of your document (eg. title).

Pandemics is build on [Pandoc](https://pandoc.org) and you should refer to the [Pandoc template documentation](https://pandoc.org/MANUAL.html#templates) for a more detailed description of templating options.

## Binary: .docx, .odt, .pptx

To style those documents, you need to provide as template another document with the same format and saved styling properties.
The contents of this reference document will be ignored, but its stylesheets (eg. title font and size) and document properties (including margins, page size, header, and footer) will be applied to the new compiled document.
See the [dedicated Pandoc documentation](https://pandoc.org/MANUAL.html#option--reference-doc) for a list of the supported styling features.

## Text based: .pdf, .html, etc.

A template is basically a boilerplate with placeholders for the content of the document you want to compile.
If you're new to this, you can first have look at the [pandoc default templates](https://github.com/jgm/pandoc-templates) or this list of [user contributed templates](https://github.com/jgm/pandoc-templates) and tweak those templates to your need.
You can also check the dedicated [Pandoc manual](https://pandoc.org/MANUAL.html#templates) to better understand how to use [Pandoc variables](https://pandoc.org/MANUAL.html#using-variables-in-templates) in your template.

[back to top](#)

# Instructions

The first thing Pandemics will look for in the recipe folder is a _recipe instructions_ file named `recipe.<format>.json` where `<format>` is the extension of the output document.

If only one instruction file is found, pandemics will automatically guess the output format. You can also decline your recipe in multiple 'flavors'. For example, in addition to a `recipe.pdf.json`, you could also have a `recipe.draft.pdf.json` that will use slightly different options for the compilation. By default, Pandemics will use the shortest one (e.g. `pdf`), if unique, over compound formats (e.g. `draft.pdf`). To use a different flavor, you will need to specify the output format explicitly (see [here]({{site.baseurl}}/tutorial/#change-the-output-format))  when publishing your document.

The recipe instruction file must be written in JSON and can specify any combination of the entries described below.

## Template path

If you are using a custom template, you must provide its path in the instructions. The path is relative to the instructions file, ie. the root of the recipe folder:

```json
{
  "template": "./pandoc-latex-template/eisvogel.tex",
}
```

Note that if no template is given, Pandemics will use the default Pandoc template.

## Pandoc options

Pandoc can be configured with a lot of different general and format specific [options](https://pandoc.org/MANUAL.html#options). In order to use those options in Pandemics, you need to list them in the recipe instruction file with the `options` label. Options can be provided either as a single string or as an array of strings .

For example, if you want to use a custom title page with the [Eisvogel recipe](https://github.com/Wandmalfarbe/pandoc-latex-template), you could create a `recipe.pdf.json` file with the following content:

```json
{
  "options":  [
        "-V titlepage=true",
        "-V titlepage-color='D8DE2C'"
  ]
}
```

## Pandoc filters

Filters are plugins that extend the functionalities of Pandoc. By default, Pandemics uses [pandoc-crossref](https://github.com/lierdakil/pandoc-crossref) and citeproc. Those filters allows to cross-reference bibliographic entries, equations, figures, and tables.

If you want to use a different set of filters, for example from [this list](https://github.com/jgm/pandoc/wiki/Pandoc-Filters), simply provide their name in the instruction file:

```json
{
  "filters":  [
        "pandoc-placetable",
        "pandoc-fignos"
  ]
}
```

Specifying such a list of filters in the instructions will override the default Pandemics filters. If you still want to use `pandoc-crossref` and `citeproc`, you have to explicitly list them in the instructions. You can also provide an empty list to disable all filters.

If you want to know more about Pandoc filters and write your own, check the [dedicated Pandoc documentation](https://pandoc.org/filters.html).

Please note that customs filters need to be available in the user path. This means that those filters need to be either:
- properly installed on the user's machine;
- provided as binaries in the recipe folder and referenced in the instructions using their relative path (beware of dependencies and cross-platform compatibility);
- made available in a Docker image to be used for compiling your recipe.

## Preprocessing

If filters should always be the preferred option, you can process the markdown document before it gets compiled by Pandoc.
This could be particularly convenient if you already have some existing software you do not want to refactor as a Pandoc filter or if you are dealing with non standard inputs formats.

By default, Pandemics applies [pandemics-include](https://gitlab.com/pandemics/pandemics-include) and [pandemics-mustache](https://www.npmjs.com/package/pandemic-mustache) tool.
This default can be overwritten by specifying a list of preprocessing steps in your instruction file:

```json
{
  "preprocessing":  [
        "markdown-pp"
  ]
}
```

This list completely override the default and you will need to explicitly list `pandemics-include` and `pandemics-mustache` if you want to use them. Like for filters, preprocessing tools need to be made available in the user path to work.

Each preprocessing step can be virtually any piece of software you want. The only constraint is that:
- it should read the source content on `stdin`
- write the modified content on `stdout`
- redirect all logging messages to `stderr`
- the last preprocessing step should output valid Markdown

All intermediate states of the content and log messages are accessible by using the `--verbose` flag when calling pandemics.

[back to top](#)

# Path management

## In templates

Pandemics will execute Pandoc in the recipe folder. This has some implications for the way you write paths to content and accessory files in your recipe.

Pandoc has a somehow inconsistent way of dealing with relative path inside the templates:

- In most of the cases, paths should be provided relative to the _root_ of the recipe folder (not relative to the template!).
For example, if your recipe contains a subfolder with a TeX template and a logo, your template should include the logo using:
  ```latex
  \includegraphics{./subfolder/the-logo.png}
  ```

- To include content from the source document folder, typically via a path passed as a Pandoc variable in the YAML header, Pandemics provides and extra Pandoc variable **`sourcedir`**. For example, to include in your TeX template a user-defined logo whose path is given relatively to the source document in the `logo` variable:
  ```latex
  \includegraphics{$sourcedir$$logo$}
  ```

- In some cases (eg. when including a css from an html template), Pandoc understand paths as relative to the source document. To allow you to load your .css file stored in your recipe, you need to use the Pandoc variable **`curdir`** which refers to the recipe directory:  
  ```html
  <link rel="stylesheet" href="$curdir$/my-sytle.css" />
  ```

## In options and preprocessors

Pandemics will execute Pandoc and preprocessing steps from the root of recipe folder, therefore all elements of the recipe can be accessed easily using relative paths.

Pandemics also provides three environment variables, **`$PANDOC_RECIPE_PATH`**, which represent the path to the recipe folder (the current working directory), and **`$PANDOC_SOURCE_PATH`** and **`$PANDOC_TARGET_PATH`**, the path to the folder were the source (the document to compile) and the target (the compiled document) are respectively located.

Typically, these variables could be used by the preprocessing steps to look for accessory files in the source folder (eg. list JSON files to use with mustache) or generate extra content along the compiled document (eg. extracted images).
Those environment variables will also be interpreted in the `options`, eg. to define HTML instructions:

```json
{
  "options": "--css $PANDOC_RECIPE_PATH/my-style.css"
}
```

[back to top](#)

# Debugging your recipe

You can display the details of what Pandemics exactly does by using the option `--verbose` when publishing.

If you want to test your recipe without having to push it constantly on a remote repo, just move your recipe to `~/.pandemics/dev/<username>/<recipename>` and put in the YAML header of your test source file:

```
pandemics:
  recipe: dev:<username>/<recipename>
```

[back to top](#)
