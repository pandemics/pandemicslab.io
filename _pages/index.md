---
layout: content
title: ""
permalink: /
---

![Focus on content / Style in a blink / Spread your ideas : Pandemics]({{ '/assets/pipeline.svg' | prepend: site.baseurl }})

<br />

Pandemics converts your markdown manuscript into a submission ready document.
Gather all the power of [pandoc](http://pandoc.org/) in a simplified framework.
Manage your templates, write without clutter, and compile your document in a blink.
