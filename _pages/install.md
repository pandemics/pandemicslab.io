---
layout: content
title: Install
permalink: /install/
---

Note that if you want to generate `.pdf` files on your local computer, you will need a full [LaTeX](https://www.latex-project.org/get/) installation on your system. LaTeX is *not* included with Pandemics.

# Brew

The simplest way to install Pandemics on Linux or Mac is using [homebrew](https://brew.sh). Once you have installed brew, type in a terminal: 

```sh
brew update
brew install pandemics
```

# NPM

If you cannot or do not want to use `brew`, you can also install Pandemics via the [Node Package Manager](https://www.npmjs.com/get-npm). After installing [Node](https://nodejs.org/en), type in a terminal:

```sh
# install Pandemic for command line use
npm install --global pandemics
```

By default, this will try to download the binaries of `pandoc` and `pandoc-crossref` within the npm package. If you have those tools already installed on your computer, you can install a bare-bone version by setting the environment variable `PANDEMICS_DEPS` to false:

```sh
# install Pandemic without any dependencies
env PANDEMICS_DEPS=FALSE npm install --global --omit=optional pandemics 
```

# Docker container

Pandemics and supporting tools are also packaged in a [dedicated Docker image](https://gitlab.com/pandemics/docker) you can use to safely offer Pandemics to your users:

```
docker run -v ${PWD}:/data/ registry.gitlab.com/pandemics/docker publish
```

Even better, you can completely automatise document publication using Continuous Integration.
Check for example the `.gitlab-ci.yml` in the [demo](https://gitlab.com/pandemics/demo) which ensures that the demo markdown document is compiled and made available to download every time it is pushed to Gitlab.

If you want to use a private recipe, do not forget to mount your ssh key to grant access to the repo:

```
docker run \
  -v ${PWD}:/data/ \
  -v ${HOME}/.ssh:/root/.ssh:ro  \
  registry.gitlab.com/pandemics/docker \
  publish
```
