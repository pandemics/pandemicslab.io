---
layout: content
title: Get started
permalink: /tutorial/
---

If you are impatient, have a look at this [demo](https://gitlab.com/pandemics/demo) and check [the PDF](https://gitlab.com/pandemics/demo/-/jobs/artifacts/master/raw/pandemics/README.pdf?job=build) Pandemics will automatically produce out of it.

# A quick example

To get started with your own document, just write some text in your favorite text editor and save it as a [Markdown](https://guides.github.com/features/mastering-markdown/) file (with the extension .md).

- If you installed Pandemics in [Atom](https://atom.io/), then click on:

  ```
  Packages > pandemics > publish
  ```
  {: style="margin: 1rem 0 1.5rem 0;"}

- If you are using [NPM](https://www.npmjs.com/get-npm), run:

  ```
  pandemics publish
  ```
  {: style="margin: 1rem 0 1.5rem 0;"}

- If you have [Docker](https://www.docker.com/products/docker-desktop):

  ```
  docker run -v ${PWD}:/data/ registry.gitlab.com/pandemics/docker \
  pandemics publish
  ```
  {: style="margin: 1rem 0 1.5rem 0;"}

Voilà! You now have next to your manuscript a `pandemics` folder with a compiled version of your manuscript.

[back to top](#)

# Change the output format

You can export your document to any format supported by [Pandoc](https://pandoc.org/): .docx, .html, .tex, etc.

## PDF

By default, Pandemics will export to .pdf format using a template best suited for a long report, eg. a thesis.
You can easily override this default by specifying the desired flavor or format in the front-matter of your markdown document.

For a short report in pdf:

```yaml
---
pandemics:
  format: short.pdf
---
```

For a handout in pdf:

```yaml
---
pandemics:
  format: handout.pdf
---
```

*TIP:* you can add a custom logo on the title page of your pdf by setting `titlepage-logo` in the YAML header:
```yaml
---
titlepage-logo: relative-path/logo.png
---
```

## HTML

You can generate a standalone webpage from your document using the `html` format. This is ideal for example to publish an online documentation (eg. with Github Pages hosting).

```yaml
---
pandemics:
  format: html
---
```

*TIP:* you can add change the highlight color by setting `main-color` to the YAML header:
```yaml
---
main-color: #00786D
---
```

## Microsoft Word

Your document can also be converted to a Word .docx format, for example to submit it for review:

```yaml
---
pandemics:
  format: docx
---
```

[back to top](#)

# Apply a styling recipe

If you want to change the look of your compiled document, you will need a `recipe`. Need to prepare a manuscript for a new journal with different formatting guidelines or citations styles? Just select another recipe! In practice, a recipe is a repository which contains a template and instructions to compile your document with a specific style. In order to apply a styling recipe to your document, simply provide the url to the recipe repository in the front-matter of your manuscript:

```yaml
---
pandemics:
  recipe: https://gitlab.com/pandemics/recipe-default-html
---
```

Have a look at our [selection]({{ site.baseurl }}/share) of recipes for more examples.

In general, a recipe will automatically decide the output format. If multiple formats are offered, you will need to also specify the desired format.

Also, you can compile your document with an older version of a given recipe simply by writing the desired version tag or commit SHA after the recipe url.

```yaml
---
pandemics:
  recipe: https://github.com/Wandmalfarbe/pandoc-latex-template  8f8fc7312f80c38ec6a3d7cc1f1e57a75be3ff4b
---
```

[back to top](#)

# Going further

Check the [writing]({{ site.baseurl }}/write/) section to discover all the features of Pandemics, like bibliographic references, figures, tables, an dynamical results integration.

If you want, you can also [create your own recipe]({{ site.baseurl }}/style/) to create documents that perfectly fit your expectations.

[back to top](#)
