---
layout: post
title: "gitlab.com/pandemics/recipe-default-docx"
categories: Recipes
link: "https://gitlab.com/pandemics/recipe-default-docx"
author: "Lionel Rigoux"
description: |
  The default recipe for DOCX format, with line numbering and double spacing.
---
