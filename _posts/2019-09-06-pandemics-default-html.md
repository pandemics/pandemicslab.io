---
layout: post
title: "gitlab.com/pandemics/recipe-default-html"
categories: Recipes
link: "https://gitlab.com/pandemics/recipe-default-html"
author: "Lionel Rigoux"
description: |
  The default recipe for HTML format, coming almost entirely from the paper Jekyll theme (Mike JS. Choi).
---
