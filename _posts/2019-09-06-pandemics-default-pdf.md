---
layout: post
title: "gitlab.com/pandemics/recipe-default-pdf"
categories: Recipes
link: "https://gitlab.com/pandemics/recipe-default-pdf"
author: "Lionel Rigoux"
description: |
  The default recipe for .pdf documents, a mere wrapper of the excellent Eisvogel template with some additional styling tweaks.

---
