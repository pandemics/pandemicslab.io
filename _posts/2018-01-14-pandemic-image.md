---
layout: post
title: "pandemics-images"
categories: Preprocessing
link: "https://gitlab.com/pandemics/pandemics-images"
author: "Lionel Rigoux"
description: |
  Extract, reformat, and rename figures.
---
