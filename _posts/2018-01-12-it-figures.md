---
layout: post
title: "it-figures"
categories: Preprocessing
link: "https://github.com/will-hart/pandemic-it-figures"
author: "Will Hart"
description: |
  A wrapper around it-figures for generating multi-panel figures.
---
